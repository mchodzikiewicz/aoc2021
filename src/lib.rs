use std::fs::File;
use std::io::Read;
use thiserror::Error;
use std::str::FromStr;

#[derive(Error, Debug)]
pub enum SubmarineError {
    #[error(transparent)]
    IOError(#[from] std::io::Error),

    #[error(transparent)]
    ParseIntError(#[from] std::num::ParseIntError),

    #[error("Direction keyword does not match any valid direction")]
    IncorrectDirection,
}

#[derive(Debug)]
pub enum Move {
    X(i32),
    Y(i32),
}

impl FromStr for Move {
    type Err = SubmarineError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut sp = s.split_whitespace();
        match sp.next().unwrap() {
            "forward" => Ok(Move::X(sp.next().unwrap().parse()?)),
            "up" => Ok(Move::Y(-sp.next().unwrap().parse()?)),
            "down" => Ok(Move::Y(sp.next().unwrap().parse()?)),
            _ => Err(SubmarineError::IncorrectDirection),
        }
    }
}

pub fn take_input(filename: &str) -> Result<String, SubmarineError> {
    let mut input = String::new();
    File::open(filename)?.read_to_string(&mut input)?;
    Ok(input)
}