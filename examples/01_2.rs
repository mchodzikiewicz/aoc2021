use itertools::Itertools;
use aoc2021::{SubmarineError, take_input};

fn main() -> Result<(),SubmarineError> {
    let input = take_input("inputs/01.txt")?;
    let result = input.split_whitespace()
        .map(|measurement| measurement.parse::<u32>().unwrap())
        .tuple_windows::<(_, _, _)>()
        .map(|(a, b, c)| a + b + c)
        .tuple_windows()
        .filter(|(a, b)| a < b)
        .count();
    println!("{:?} measurements are larger than previous measurement", result);
    Ok(())

}