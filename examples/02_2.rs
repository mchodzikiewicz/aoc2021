use std::str::FromStr;
use aoc2021::{Move, SubmarineError, take_input};

fn main() -> Result<(), SubmarineError> {
    let input = take_input("inputs/02.txt")?;
    let (x, y, _) = input.split_terminator("\n")
        .map(|s| Move::from_str(s)?)
        .fold((0, 0, 0),
              |(x, y, a), m| match m {
                  Move::X(val) => (x + val, y + val * a, a),
                  Move::Y(val) => (x, y, a + val),
              },
        );

    println!("Horizontal: {:?} Depth: {}, multiplied gives: {}", x, y, x * y);
    Ok(())
}